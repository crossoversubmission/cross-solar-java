package com.crossover.techtrial.util;

import java.time.LocalDateTime;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.repository.HourlyElectricityRepository;
import com.crossover.techtrial.repository.PanelRepository;

@Component
public class GenerateData implements CommandLineRunner{

  @Autowired
  PanelRepository pRepository;
  
  @Autowired
  HourlyElectricityRepository hRepository;
  
  @Override
  public void run(String... args) throws Exception {
    
    Integer rand = new Random().nextInt();
    String serial = String.valueOf(rand < 0 ? rand*-1 : rand);
    Panel panel = new Panel(serial, 54.123232, 54.123232, "B" + serial);
    pRepository.save(panel);
    
    LocalDateTime day1 = LocalDateTime.now().minusDays(1);
    LocalDateTime day2 = LocalDateTime.now().minusDays(2);
    LocalDateTime day3 = LocalDateTime.now().minusDays(3);
    
    hRepository.save(new HourlyElectricity(panel, 10L, day1));
    hRepository.save(new HourlyElectricity(panel, 11L, day1));
    hRepository.save(new HourlyElectricity(panel, 12L, day1));
    hRepository.save(new HourlyElectricity(panel, 13L, day1));
    hRepository.save(new HourlyElectricity(panel, 14L, day1));
    hRepository.save(new HourlyElectricity(panel, 15L, day2));
    hRepository.save(new HourlyElectricity(panel, 16L, day2));
    hRepository.save(new HourlyElectricity(panel, 17L, day2));
    hRepository.save(new HourlyElectricity(panel, 18L, day2));
    hRepository.save(new HourlyElectricity(panel, 19L, day2));
    hRepository.save(new HourlyElectricity(panel, 20L, day3));
    hRepository.save(new HourlyElectricity(panel, 21L, day3));
    hRepository.save(new HourlyElectricity(panel, 22L, day3));
    hRepository.save(new HourlyElectricity(panel, 23L, day3));
    hRepository.save(new HourlyElectricity(panel, 24L, day3));
  } 
}
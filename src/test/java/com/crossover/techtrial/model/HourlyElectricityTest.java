package com.crossover.techtrial.model;

import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * HourlyElectricityTest class will test all APIs in HourlyElectricity.java.
 * 
 * @author Crossover
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HourlyElectricityTest {
  
  @Test
  public void testEqualsAndHashCode() {
    
    Panel panel1 = new Panel("1111", 111.11, 111.11, "aaaaa");

    HourlyElectricity electricity1 = new HourlyElectricity();
    electricity1.setPanel(panel1);
    electricity1.setGeneratedElectricity(10L);
    electricity1.setReadingAt(LocalDateTime.now());
    
    HourlyElectricity electricity2 = new HourlyElectricity();
    electricity2.setPanel(panel1);
    electricity2.setGeneratedElectricity(10L);
    electricity2.setReadingAt(LocalDateTime.now());
    
    assertTrue(electricity1.equals(electricity2) && electricity2.equals(electricity1));
    assertTrue(electricity1.hashCode() == electricity2.hashCode());
  }
}
